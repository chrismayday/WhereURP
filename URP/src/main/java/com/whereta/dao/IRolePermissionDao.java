package com.whereta.dao;

import com.whereta.model.RolePermission;

import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 17:25
 */
public interface IRolePermissionDao {

    Set<Integer> getPermissionIdSetByRoleId(int roleId);

    void deleteByPerId(int perId);

    void deleteByRoleId(int roleId);

    int addRolePermission(RolePermission rolePermission);
}
